'''
                                  ESP Health
                         Notifiable Diseases Framework
                            Diagnosed Hypertension   


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2019 Commonwealth Informatics, Inc.
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.  
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
import datetime, gc, pdb
from datetime import timedelta
from collections import defaultdict, OrderedDict
from dateutil.relativedelta import relativedelta
from ESP.utils import log
from ESP.utils.utils import queryset_iterator
from django.db import IntegrityError
from django.db.models import F,Max,Q
from django.contrib.contenttypes.models import ContentType

from ESP.emr.models import Encounter, Patient
from ESP.hef.models import Event
from ESP.hef.base import PrescriptionHeuristic,BaseEventHeuristic
from ESP.hef.base import DiagnosisHeuristic, Dx_CodeQuery, HEF_CORE_URI
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.static.models import DrugSynonym

SBP_LIM=140
DBP_LIM=90

class EncounterBPHeuristic(BaseEventHeuristic):
    '''
    The other Hypertension plugin does highbp.  
    This does noy_highbp 
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name

    @property
    def short_name(self):
        return u'enc:%s' % self.name

    uri = u'urn:x-esphealth:heuristic:channing:encbp:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def encounters(self):
        if self.name=='not_highbp':
            sbp_q=Q(bp_systolic__lt = SBP_LIM)
            dbp_q=Q(bp_diastolic__lt = DBP_LIM, )
            hypr_q=Q(patient__case__condition = 'diagnosedhypertension')
            dt_q=Q(patient__case__date__lt = F('date'))
            q_obj=sbp_q & dbp_q & hypr_q & dt_q
        elif self.name=='highbp':
            sbp_q=Q(bp_systolic__gte = SBP_LIM)
            dbp_q=Q(bp_diastolic__gte = DBP_LIM, )
            q_obj=sbp_q | dbp_q
        return Encounter.objects.filter(q_obj).only('id','bp_systolic','date','events__name','patient','patient__date_of_birth','provider')

    @property
    def enc_event_name(self):
        return 'enc:%s' % self.name 

    @property
    def event_names(self):
        return [self.enc_event_name]

    def generate(self):
        enc_qs = self.encounters.exclude(events__name__in=self.event_names)
        log.info('Generating events for "%s"' % self)
        counter = 0
        for enc in queryset_iterator(enc_qs):
            Event.create(name = self.enc_event_name,
                    source=self.uri,
                    patient = enc.patient,
                    date = enc.date,
                    provider = enc.provider,
                    emr_record = enc,
                    )
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class DiagnosedHypertension(DiseaseDefinition):
    '''
    DiagnosedHypertension
    '''
    
    conditions = ['diagnosedhypertension']
    
    uri = 'urn:x-esphealth:disease:commoninf:diagnosedhypertension:v1'
    
    short_name = 'diagnosedhypertension'
    
    test_name_search_strings = [
        
        ]
    
    timespan_heuristics = []

    
    def update_drugsyn(self):
        druglist=(('hydrochlorothiazide','hydrochlorothiazide','self'),
                  ('hydrochlorothiazide','hctz',None),
                  ('hydrochlorothiazide','esidrix',None),
                  ('hydrochlorothiazide','oretic',None),
                  ('hydrochlorothiazide','microzide',None),
                  ('hydrochlorothiazide','hydrodiuril',None),
                  ('chlorthalidone','chlorthalidone','self'),
                  ('chlorthalidone','thalitone',None),
                  ('indapamide','indapamide','self'),
                  ('indapamide','lozol',None),
                  ('amlodipine','amlodipine','self'),
                  ('amlodipine','norvasc',None),
                  ('clevidipine','clevidipine','self'),
                  ('clevidipine','cleviprex',None),
                  ('felodipine','felodipine','self'),
                  ('felodipine','plendil',None),
                  ('isradipine','isradipine','self'),
                  ('isradipine','dynacirc',None),
                  ('nicardipine','nicardipine','self'),
                  ('nicardipine','cardene',None),
                  ('nifedipine','nifedipine','self'),
                  ('nifedipine','procardia',None),
                  ('nifedipine','adalat',None),
                  ('nisoldipine','nisoldipine','self'),
                  ('nisoldipine','sular',None),
                  ('diltiazem','diltiazem','self'),
                  ('diltiazem','cardizem',None),
                  ('diltiazem','cartia',None),
                  ('diltiazem','diltia',None),
                  ('diltiazem','diltzac',None),
                  ('diltiazem','tiazac',None),
                  ('diltiazem','taztia',None),
                  ('verapamil','verapamil','self'),
                  ('verapamil','isoptin',None),
                  ('verapamil','calan',None),
                  ('verapamil','covera',None),
                  ('verapamil','verelan',None),
                  ('acebutolol','acebutolol','self'),
                  ('acebutolol','sectral',None),
                  ('atenolol','atenolol','self'),
                  ('atenolol','tenormin',None),
                  ('betaxolol','betaxolol','self'),
                  ('betaxolol','kerlone',None),
                  ('bisoprolol','bisoprolol','self'),
                  ('bisoprolol','zebeta',None),
                  ('carvedilol','carvedilol','self'),
                  ('carvedilol','coreg',None),
                  ('labetolol','labetolol','self'),
                  ('labetolol','trandate',None),
                  ('metoprolol','metoprolol','self'),
                  ('metoprolol','lopressor',None),
                  ('nadolol','nadolol','self'),
                  ('nadolol','corgard',None),
                  ('nebivolol','nebivolol','self'),
                  ('nebivolol','bystolic',None),
                  ('pindolol','pindolol','self'),
                  ('pindolol','visken',None),
                  ('propranolol','propranolol','self'),
                  ('propranolol','inderal',None),
                  ('benazepril','benazepril','self'),
                  ('benazepril','lotensin',None),
                  ('catopril','catopril','self'),
                  ('catopril','capoten',None),
                  ('enalapril','enalapril','self'),
                  ('enalapril','enalaprilat',None),
                  ('enalapril','vasotec',None),
                  ('fosinopril','fosinopril','self'),
                  ('fosinopril','monopril',None),
                  ('lisinopril','lisinopril','self'),
                  ('lisinopril','prinivil',None),
                  ('lisinopril','zestril',None),
                  ('moexipril','moexipril','self'),
                  ('moexipril','univasc',None),
                  ('perindopril','perindopril','self'),
                  ('perindopril','aceon',None),
                  ('quinapril','quinapril','self'),
                  ('quinapril','accupril',None),
                  ('ramipril','ramipril','self'),
                  ('ramipril','altace',None),
                  ('trandolapril','trandolapril','self'),
                  ('trandolapril','mavik',None),
                  ('candesartan','candesartan','self'),
                  ('candesartan','atacand',None),
                  ('eprosartan','eprosartan','self'),
                  ('eprosartan','teveten',None),
                  ('irbesartan','irbesartan','self'),
                  ('irbesartan','avapro',None),
                  ('losartan','losartan','self'),
                  ('losartan','cozaar',None),
                  ('olmesartan','olmesartan','self'),
                  ('olmesartan','benicar',None),
                  ('telmisartan','telmisartan','self'),
                  ('telmisartan','micardis',None),
                  ('valsartan','valsartan','self'),
                  ('valsartan','diovan',None),
                  ('clonidine','clonidine','self'),
                  ('clonidine','catapres',None),
                  ('doxazosin','doxazosin','self'),
                  ('doxazosin','cardura',None),
                  ('guanfacine','guanfacine','self'),
                  ('guanfacine','tenex',None),
                  ('methyldopa','methyldopa','self'),
                  ('methyldopa','aldomet',None),
                  ('prazosin','prazosin','self'),
                  ('prazosin','minipress',None),
                  ('terazosin','terazosin','self'),
                  ('terazosin','hytrin',None),
                  ('eplerenone','eplerenone','self'),
                  ('eplerenone','inspra',None),
                  ('sprinolactone','sprinolactone','self'),
                  ('sprinolactone','aldactone',None),
                  ('aliskiren','aliskiren','self'),
                  ('aliskiren','tekturna',None),
                  ('hydralazine','hydralazine','self'),
                  ('hydralazine','apresoline',None))
        for syntup in druglist:
               drugrow, created = DrugSynonym.objects.get_or_create(generic_name=syntup[0],other_name=syntup[1],comment=syntup[2])
               if created:
                   drugrow.save()
                   
    @property
    def event_heuristics(self):
        
        self.update_drugsyn()
        
        heuristic_list = []
        #
        # Not high blood pressure
        #
        heuristic_list.append( EncounterBPHeuristic(
            name = 'not_highbp'
            ))
        #
        # Diagnosis Codes
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'hypertension',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='I15', type='icd10'),
                Dx_CodeQuery(starts_with='405.', type='icd9'),
                Dx_CodeQuery(starts_with='I10', type='icd10'),
                Dx_CodeQuery(starts_with='401.', type='icd9'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'pregnancy',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='V22', type='icd9'),
                Dx_CodeQuery(starts_with='V23', type='icd9'),
                Dx_CodeQuery(starts_with='Z33', type='icd10'),
                Dx_CodeQuery(starts_with='Z34', type='icd10'),
                Dx_CodeQuery(starts_with='O09', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'endstagerenal',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='585.6', type='icd9'),
                Dx_CodeQuery(starts_with='N18.6', type='icd10'),
                ]
            ))
        
        #
        # Prescriptions
        #
        heuristic_list.append( PrescriptionHeuristic(
            name = 'hydrochlorothiazide',
            drugs = DrugSynonym.generics_plus_synonyms(['Hydrochlorothiazide', ]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'chlorthalidone',
            drugs = DrugSynonym.generics_plus_synonyms(['Chlorthalidone' ]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'indapamide',
            drugs =  DrugSynonym.generics_plus_synonyms(['Indapamide']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'amlodipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Amlodipine',]),
            ))    
        heuristic_list.append( PrescriptionHeuristic(
            name = 'clevidipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Clevidipine',]),
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'felodipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Felodipine',]),
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'isradipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Isradipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nicardipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nicardipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nifedipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nifedipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nisoldipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nisoldipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'diltiazem',
            drugs =  DrugSynonym.generics_plus_synonyms(['Diltiazem',]),
            ))       
        heuristic_list.append( PrescriptionHeuristic(
            name = 'verapamil',
            drugs =  DrugSynonym.generics_plus_synonyms(['Verapamil',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'acebutolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Acebutolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'atenolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Atenolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'betaxolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Betaxolol']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'bisoprolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Bisoprolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'carvedilol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Carvedilol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'labetolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Labetolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'metoprolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Metoprolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nadolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nadolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nebivolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nebivolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'pindolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Pindolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'propranolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Propranolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'benazepril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Benazepril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'catopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Catopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'enalapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Enalapril']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fosinopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Fosinopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'lisinopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Lisinopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'moexipril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Moexipril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'perindopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['perindopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'quinapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Quinapril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'ramipril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Ramipril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'trandolapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Trandolapril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'candesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Candesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'eprosartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Eprosartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'irbesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Irbesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'losartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Losartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'olmesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Olmesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'telmisartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Telmisartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'valsartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Valsartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'clonidine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Clonidine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'doxazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Doxazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'guanfacine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Guanfacine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'methyldopa',
            drugs =  DrugSynonym.generics_plus_synonyms(['Methyldopa',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'prazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Prazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'terazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Terazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'eplerenone',
            drugs =  DrugSynonym.generics_plus_synonyms(['Eplerenone',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'sprinolactone',
            drugs =  DrugSynonym.generics_plus_synonyms(['Sprinolactone',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'aliskiren',
            drugs =  DrugSynonym.generics_plus_synonyms(['Aliskiren',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'hydralazine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Hydralazine',]),
            ))
        return heuristic_list
        
    def exclude_2nd_list(self,full_list,exclude_list):
        return list(set(full_list)-set(exclude_list))
        
    
    def update_case_active_status(self):
        pre_updatable_qs = Case.objects.filter(condition=self.conditions[0]).annotate(max_hdate=Max('caseactivehistory__latest_event_date')).annotate(max_edate=Max('events__date'))
        pre_updatable_qs = pre_updatable_qs.annotate(max_adate=Max('caseactivehistory__date'))
        q1 = Q(max_edate__gt=F('max_hdate')) # events after current case history max event date
        q2 = Q(max_edate__lt=datetime.datetime.now().date() - relativedelta(years=2)) #or last event more than two years ago
        updatable_qs = pre_updatable_qs.filter(q1 | q2)
        counter = 0 # track number of cases updated. Not currently bothering with case history last event updates, only active status changes.
        for case in updatable_qs:
            cur_stat=case.caseactivehistory_set.get(date=case.max_adate).status
            last_date = case.max_hdate
            events = case.events.order_by('date')
            if case.isactive:
                updtcurhist = False
                last_bp_ok = None
                last_bp_dt = None
                last_dx_dt = None
                last_rx_dt = None
                new_stat = None
                age = None
                for event in events:
                    if event.patient.date_of_birth:
                        age = event.patient.get_age(event.date).years
                    #patients over 85 can't have diagnosed hypertension
                    if age > 85:
                        case.isactive = False
                        case.inactive_date = case.patient.date_of_birth.date() + relativedelta(years=85)
                        case.save()
                        try:
                            newhist = CaseActiveHistory.objects.create(case=case,
                                                    date=case.inactive_date,
                                                    status='D',
                                                    change_reason='D',
                                                    latest_event_date=event.date,
                                                    content_type=ContentType.objects.get(model='patient'),
                                                    object_id=case.patient.id,
                                                    content_object=case.patient)
                            newhist.save()
                        except IntegrityError:
                            newhist = CaseActiveHistory.objects.get(case=case,
                                                    date=case.inactive_date)
                            newhist.status='D'
                            newhist.change_reason='D'
                            newhist.latest_event_date=event.date
                            newhist.content_type=ContentType.objects.get(model='patient')
                            newhist.object_id=case.patient.id
                            newhist.content_object=case.patient
                            newhist.save()
                        counter += 1
                        break
                    if event.name in ['enc:not_highbp','enc:highbp']:
                        last_bp_date=event.date
                        last_bp_ok = True if event.name=='enc:not_highbp' else False
                    last_rx_dt = event.date if event.name.startswith('rx:') else last_rx_dt
                    last_dx_dt = event.date if event.name.startswith('dx:') else last_dx_dt
                    if case.max_hdate < event.date:
                        if last_bp_dt and last_bp_dt < event.date <= last_date + relativedelta(years=2) and not cur_stat=='UK': 
                            new_stat = 'UK'
                            break
                        elif last_rx_dt and last_dx_dt and event.date >= last_dx_dt + relativedelta(years=2) and event.date >= last_rx_dt + relativedelta(years=2) and last_bp_ok: 
                            new_stat = 'D'
                            break  
                        elif not cur_stat=='C' and last_bp_ok:
                            new_stat = 'C'
                            break
                        elif not cur_stat=='U' and not last_bp_ok and last_bp_ok is not None:
                            new_stat = 'U'
                            break
                        else:
                            updtcurhist = True
                            updtevnt = event
                            last_date = event.date
                if updtcurhist: 
                    histupdt=CaseActiveHistory.objects.get(case=case,date=case.max_adate) 
                    histupdt.latest_event_date=last_date
                    histupdt.content_type = ContentType.objects.get_for_model(updtevnt)
                    histupdt.object_id = updtevnt.pk
                    histupdt.save()
                if new_stat: 
                    newhist = CaseActiveHistory.create(case,
                                                       new_stat,
                                                       event.date,
                                                       'E' if new_stat == 'UK' else None or 'Q' if new_stat in ['C','U'] else None or 'D',
                                                       event)    
                    if new_stat=='D': 
                        case.isactive = False
                        case.inactive_date = event.date
                        case.save()
                    counter += 1
            else:
                events = events.filter(date__gt=case.max_hdate).order_by('date')
                counter += self.create_or_update_case(events,case)
        return counter

    def create_or_update_case(self,event_itrbl,case=None):
        exclusions = ['dx:pregnancy','dx:endstagerenal']
        last_bp=None
        for event in event_itrbl: #assuming the list is orderd by date -- should be true
            if event.name in ['enc:not_highbp','enc:highbp']:
                last_bp=event
            if event.name.startswith('dx:hypertension'):
                #hypertension only for 18-85
                age = event.patient.get_age(event.date)
                if 18 <= age.years if age is not None else 0 <= 85:
                    exclude_if_present= BaseEventHeuristic.get_events_by_name(name=exclusions).filter(patient=event.patient,date__lte=event.date)
                    if exclude_if_present:
                        for excl in exclude_if_present:
                            if excl.name.startswith('dx:endstagerenal'):
                                return 0
                            elif excl.name.startswith('dx:pregnant') and excl.date >= event.date - relativedelta('9 months'):
                                continue
                    t = False
                    if case==None:
                        t, case = self._create_case_from_event_list(
                            condition = self.conditions[0],
                            criteria = 'Dx hypertension',
                            recurrence_interval = None,
                            event_obj = event,
                            relevant_event_names = event_itrbl,
                            )
                        log.info('Created new hypertension case: %s' % case)
                    case.isactive=True
                    case.inactive_date=None
                    case.save()
                    # #diagnosedhypertension will have four status values: U (uncontrolled) C (controlled), UK (unknown), D (deactivated)
                    CaseActiveHistory.create(case,
                                         ('C' if last_bp and last_bp.name=='enc:not_highbp' else 'U'),
                                         event.date,
                                         'Q',
                                         event)
                    return 1
        return 0
    
    def _add_events_to_existing_cases(self,patients,events): 
        #I'm creating this locally because the nodis/base version takes events and creates a new event dict, which doubles memory reqs
        case_patients = set()
        existing_cases = Case.objects.filter(
            patient__in = patients,
            condition = self.conditions[0],
            )
        for case in existing_cases:
            case_patients.add(case.patient)
            self._update_case_from_event_list(case,
                            relevant_events = events.get(case.patient, []))
        return case_patients
    
    def generate_hypertension(self):
    #
    #
           
        dx_ev_names = ['dx:hypertension']
        rx_ev_names = [
            'rx:hydrochlorothiazide',
            'rx:chlorthalidone',
            'rx:indapamide',
            'rx:amlodipine',
            'rx:clevidipine',
            'rx:felodipine',
            'rx:isradipine',
            'rx:nicardipine',
            'rx:nifedipine',
            'rx:nisoldipine',
            'rx:diltiazem',
            'rx:verapamil',
            'rx:acebutolol',
            'rx:atenolol',
            'rx:betaxolol',
            'rx:bisoprolol',
            'rx:carvedilol',
            'rx:labetolol',
            'rx:metoprolol',
            'rx:nadolol',
            'rx:nebivolol',
            'rx:pindolol',
            'rx:propranolol',
            'rx:benazepril',
            'rx:catopril',
            'rx:enalapril',
            'rx:fosinopril',
            'rx:lisinopril',
            'rx:moexipril',
            'rx:perindopril',
            'rx:quinapril',
            'rx:ramipril',
            'rx:trandolapril',
            'rx:candesartan',
            'rx:eprosartan',
            'rx:irbesartan',
            'rx:losartan',
            'rx:olmesartan',
            'rx:telmisartan',
            'rx:valsartan',
            'rx:clonidine',
            'rx:doxazosin',
            'rx:guanfacine',
            'rx:methyldopa',
            'rx:prazosin',
            'rx:terazosin',
            'rx:eplerenone',
            'rx:sprinolactone',
            'rx:aliskiren',
            'rx:hydralazine',
        ]
        enc_ev_names=['enc:highbp','enc:not_highbp']
        all_ev_names=dx_ev_names+rx_ev_names+enc_ev_names

        #get distinct patients 
        patient_ids = set()
        #get all unattached hypertension events.
        raw_qslst = BaseEventHeuristic.get_events_by_name(name=all_ev_names).values_list('pk',flat=True)
        excl_qslst = Event.objects.filter(case__condition=self.conditions[0]).values_list('pk',flat=True)
        all_qs_filter = self.exclude_2nd_list(raw_qslst,excl_qslst)
        all_qs = Event.objects.filter(pk__in=all_qs_filter).order_by('patient','date')
        counter = 0
        event_dict={}
        for event in all_qs.iterator(): #this will limit python memory reqs by avoiding the queryset cache
            patient_ids.add(event.patient)
            if len(patient_ids)==5001:
                #because of memory constraints, we use 5K patient chunks
                #I'm sure there's a more artful way to do this, but for testing this will do...
                log.info('Updating existing hypertension cases with new events')
                patients_with_existing_cases = self._add_events_to_existing_cases(event_dict.keys(), event_dict)
                log.info('Generating new cases for hypertension')
                for patient, events in event_dict.iteritems():
                    if patient in patients_with_existing_cases:
                        continue
                    else:
                        counter += self.create_or_update_case(events)
                #now reset the set and dict objects
                patient_ids = set()
                event_dict={}
                patient_ids.add(event.patient)
 
            try:
                event_dict[event.patient].append(event)
            except KeyError:
                event_dict[event.patient] = [event]

        #deal with the last <5K patients -- again, not so artful, but just testing for now...                
        log.info('Updating existing hypertension cases with new events')
        patients_with_existing_cases = self._add_events_to_existing_cases(event_dict.keys(), event_dict)

        log.info('Generating new cases for hypertension')
        counter = 0
        for patient, events in event_dict.iteritems():
            if patient in patients_with_existing_cases:
                continue
            else:
                counter += self.create_or_update_case(events)

        return counter # Count of new cases

    def generate(self):
        log.info('Generating cases of %s' % self.short_name)
        counter = 0
        counter += self.generate_hypertension()
        log.debug('Generated %s new cases of %s' % (counter,self.short_name))
        log.debug('Updating case active status values for %s' % (self.short_name))
        updates = self.update_case_active_status()
        log.debug('Updated case active status with %s updates for %s' % (updates,self.short_name))
        return counter # Count of new cases
    
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

def event_heuristics():
    return DiagnosedHypertension().event_heuristics

def disease_definitions():
    return [DiagnosedHypertension()]
